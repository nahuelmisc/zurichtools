﻿
namespace ZurichTools.Logic
{
    public interface IAltaScoringIDInfoAutoLogic
    { 
        string GenerarScript(string ultimoScoringID, string entrada);
    }
}
