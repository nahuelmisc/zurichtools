﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZurichTools.Logic
{
    public interface IModificacionAgenciaUsuarioLogic
    {
        string GenerarScript(string p1, string p2);
    }
}
