﻿

namespace ZurichTools.Logic
{
    public interface IAsignarPlanesLogic
    {
        string GenerarScript(string nombrePlan, string telemas);
    }
}
