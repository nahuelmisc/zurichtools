﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZurichTools.Logic
{
    public interface ILineComparerLogic
    {
        string RetornarComparacion(string entrada, string textoAComparar, bool coincidirMayusMinus);
    }
}
