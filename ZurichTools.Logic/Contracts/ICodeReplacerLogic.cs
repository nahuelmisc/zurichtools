﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZurichTools.Logic
{
    public interface ICodeReplacerLogic
    {
        string RetornarCodigoConReemplazo(string textoEntrada, string bloqueCodigo, string tag, bool agregarEspacios);
    }
}
