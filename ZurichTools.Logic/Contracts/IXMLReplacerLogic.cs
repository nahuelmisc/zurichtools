﻿
using System.Collections.Generic;
namespace ZurichTools.Logic
{
    public interface IXMLReplacerLogic
    {
        string ReemplazarValores(string xmlWithValues, string xmlWithoutValues);

        List<string> ObtenerListaTags(string xmlString);
    }
}
