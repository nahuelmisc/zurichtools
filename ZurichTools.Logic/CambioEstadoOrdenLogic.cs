﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;
using ZurichTools.Entities;

namespace ZurichTools.Logic
{
    public class CambioEstadoOrdenLogic : ICambioEstadoOrdenLogic
    {
        public string RetornarCodigo(string entrada, EstadoOrden estadoOrdenSelected)
        {
            this.Validate(entrada);

            StringBuilder salidaStringBuilder = new StringBuilder();

            String[] lines = entrada.Split('\n');

            string codigo = string.Empty;

            lines = ZurichToolsTextUtils.RemoveEmptyLines(lines);

            foreach (var l in lines)
            {
                String line = l.Trim();

                codigo = String.Format(Parameters.UpdateTSetWAsXWhereYEqualsVarcharZ, Parameters.TablaOrden, Parameters.CampoEstadoID, estadoOrdenSelected.Id, Parameters.CampoOrdenNumeroPresupuesto, line);

                salidaStringBuilder.AppendLine(codigo);
            }

            return salidaStringBuilder.ToString();
        }

        private void Validate(string entrada)
        {
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (entrada.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Entrada)));
            }

            if (listaValidaciones.Any())
                throw new ValidationException(listaValidaciones);
        } 
    }
}
