﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;

namespace ZurichTools.Logic
{
    public class LineComparerLogic : ILineComparerLogic
    {
        public string RetornarComparacion(string entrada, string textoAComparar, bool coincidirMayusMinus)
        {
            this.Validate(entrada, textoAComparar);

            StringBuilder textoSalidaStringBuilder = new StringBuilder();

            List<string> lineasTextoEntrada = ZurichToolsTextUtils.RemoveEmptyLines(entrada.Split('\n')).ToList();

            List<string> lineasTextoAComparar = ZurichToolsTextUtils.RemoveEmptyLines(textoAComparar.Split('\n')).ToList();

            List<string> lineasDiferencia = new List<string>();

            if (coincidirMayusMinus)
            {
                lineasDiferencia = lineasTextoAComparar.Where(lt => !lineasTextoEntrada.Any(le => le.ToUpper().Equals(lt.ToUpper()))).ToList();
            }
            else
            {
                lineasDiferencia = lineasTextoAComparar.Where(lt => !lineasTextoEntrada.Any(le => le.Equals(lt))).ToList();
            } 

            lineasDiferencia.ForEach(linea => textoSalidaStringBuilder.Append(linea)); 

            return textoSalidaStringBuilder.ToString();
        }


        private void Validate(string TextoEntrada, string TextoComparar)
        {            
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (TextoEntrada.Trim().Equals(String.Empty))
            { 
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Entrada)));
            }
            
            if (TextoComparar.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Comparar)));
            }
            
            if (TextoComparar.Length < TextoEntrada.Length)
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeSerMasGrandeQueElCampoY, Labels.Entrada, Labels.Comparar)));
            }

            if (listaValidaciones.Any())
                throw new ValidationException(listaValidaciones);
        }
    }
}
