﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;

namespace ZurichTools.Logic
{
    public class CodeReplacerLogic : ICodeReplacerLogic
    {
        public string RetornarCodigoConReemplazo(string textoEntrada, string bloqueCodigo, string tag, bool agregarEspacios)
        {
            this.Validate(textoEntrada, bloqueCodigo, tag);

            StringBuilder salidaStringBuilder = new StringBuilder();

            String[] lines = ZurichToolsTextUtils.RemoveEmptyLines(textoEntrada.Split('\n'));

            foreach (var l in lines)
            {
                String codigo = bloqueCodigo;
                String line = l.Trim();

                codigo = codigo.Replace(tag, line);

                salidaStringBuilder.AppendLine(codigo);

                if (agregarEspacios)
                    salidaStringBuilder.AppendLine();
            }

            return salidaStringBuilder.ToString();
        }

        private void Validate(string textoEntrada, string bloqueCodigo, string tag)
        {
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (textoEntrada.Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Entrada)));
            }

            if (tag.Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.TagReemplazo)));
            }
            else if (bloqueCodigo.Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.BloqueCodigo)));
            }
            else if (!bloqueCodigo.Contains(tag))
            {
                listaValidaciones.Add(new ValidationResult(false, Messages.ElTagDeReemplazoIngresadoNoSeEncuentraEnElBloque));
            }

            if (listaValidaciones.Any())
                throw new ValidationException(listaValidaciones);
        }
    }
}
