﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;

namespace ZurichTools.Logic
{
    public class AsignarPlanesLogic : IAsignarPlanesLogic
    {
        public string GenerarScript(string nombrePlan, string telemas)
        { 
            StringBuilder salida = new StringBuilder();

            string codigoAsignarPlan = FileScriptProvider.GetTextScriptFile(Parameters.KeyFileAsignarPlanes);

            string usuariosTelemas = ZurichToolsTextUtils.GenerarCadenaInsertMultiple(telemas, ZurichTools.CrossCutting.ZurichToolsTextUtils.TipoCadena.Varchar);

            codigoAsignarPlan = codigoAsignarPlan.Replace(Parameters.TAGPLANDESCRIPCION, nombrePlan);

            codigoAsignarPlan = codigoAsignarPlan.Replace(Parameters.TAGUSUARIOS, usuariosTelemas);

            salida.Append(codigoAsignarPlan);

            salida.AppendLine();

            return salida.ToString();
        }
    }
}
