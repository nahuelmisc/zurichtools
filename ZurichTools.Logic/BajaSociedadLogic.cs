﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;

namespace ZurichTools.Logic
{
    public class BajaSociedadLogic : IBajaSociedadLogic
    {
        public string GenerarScript(string entrada)
        {
            this.Validate(entrada);

            string codigoAsignarBajaSoc = FileScriptProvider.GetTextScriptFile(Parameters.KeyFileAsignarBajaSoc);

            codigoAsignarBajaSoc = codigoAsignarBajaSoc.Replace(Parameters.TAGIDSOC, entrada);

            return codigoAsignarBajaSoc;
        }

        private void Validate(string entrada)
        {
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (entrada.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Entrada)));
            }

            if (listaValidaciones.Any())
                throw new ValidationException(listaValidaciones);
        }
    }
}