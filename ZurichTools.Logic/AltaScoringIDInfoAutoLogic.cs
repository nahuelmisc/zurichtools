﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;

namespace ZurichTools.Logic
{
    public class AltaScoringIDInfoAutoLogic : IAltaScoringIDInfoAutoLogic
    { 
        public string GenerarScript(string ultimoScoringID, string entrada)
        {
            StringBuilder salida = new StringBuilder();

            string codigosInfoAuto = ZurichToolsTextUtils.GenerarCadenaInsertMultiple(entrada, ZurichTools.CrossCutting.ZurichToolsTextUtils.TipoCadena.Varchar);

            string codigoAltaScoringIDInfoAuto = FileScriptProvider.GetTextScriptFile(Parameters.KeyFileAltaScoringIDInfoAuto);

            codigoAltaScoringIDInfoAuto = codigoAltaScoringIDInfoAuto.Replace(Parameters.TAGULTIMOTIPOSCORINGVALORID, ultimoScoringID);

            codigoAltaScoringIDInfoAuto = codigoAltaScoringIDInfoAuto.Replace(Parameters.TAGCODIGOSINFOAUTO, codigosInfoAuto);

            salida.Append(codigoAltaScoringIDInfoAuto);

            salida.AppendLine();

            return salida.ToString();
        }

        private void Validate(string ultimoScoringID, string entrada)
        {
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (ultimoScoringID.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.UltimoScoringID)));
            }

            if (entrada.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.CodigosInfoAuto)));
            }

            if (listaValidaciones.Any())
                throw new ValidationException(listaValidaciones);
        }
    }
}
