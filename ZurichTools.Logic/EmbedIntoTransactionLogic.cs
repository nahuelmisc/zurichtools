﻿using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;

namespace ZurichTools.Logic
{
    public class EmbedIntoTransactionLogic : IEmbedIntoTransactionLogic
    {
        public string GenerarScript(string entrada)
        { 
            string transScriptText = FileScriptProvider.GetTextScriptFile(Parameters.KeyFileTransactionScript);

            string textoScriptSinTransaction = entrada;

            string textoScriptConTransaction = transScriptText.Replace(Parameters.TAGTransScript, entrada);

            return textoScriptConTransaction;
        }
    }
}
