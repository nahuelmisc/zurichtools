﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;

namespace ZurichTools.Logic
{
    public class ModificacionAgenciaUsuarioLogic : IModificacionAgenciaUsuarioLogic
    {
        public string GenerarScript(string agenciaNombre, string telemas)
        {
            string textoScript = FileScriptProvider.GetTextScriptFile(Parameters.KeyFileModificarAgenciaUsuarioScript);

            textoScript = textoScript.Replace(Parameters.TAGDESCRIPCION, agenciaNombre);

            string usuarios = ZurichToolsTextUtils.GenerarCadenaInsertMultiple(telemas, ZurichTools.CrossCutting.ZurichToolsTextUtils.TipoCadena.Varchar);

            textoScript = textoScript.Replace(Parameters.TAGUSUARIOS, usuarios);

            return textoScript;
        }

        public void Validate(string agenciaNombre, string telemas)
        {
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (agenciaNombre.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Agencia)));
            }

            if (telemas.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Usuarios)));
            }

            if (listaValidaciones.Any())
                throw new ValidationException(listaValidaciones);
        }
    }
}
