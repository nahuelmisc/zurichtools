﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using ZurichTools.Entities;

namespace ZurichTools.Logic
{
    public class XMLReplacerLogic : IXMLReplacerLogic
    {
        private const string inicioTag = "</";

        private const string finTag = ">";

        public string ReemplazarValores(string xmlWithValues, string xmlWithoutValues)
        {
            List<string> listaTags = ObtenerListaTags(xmlWithValues);

            List<ZurichToolsXMLNode> listaNodos = ObtenerListaNodosConValores(listaTags, xmlWithValues);

            List<string> listaLineasXMLConValoresReemplazados = SetearValoresEnXMLSinValores(listaNodos, xmlWithoutValues);

            StringBuilder salida = new StringBuilder();

            foreach (var linea in listaLineasXMLConValoresReemplazados)
            {
                salida.Append(linea);
            }

            return salida.ToString();
        }

        private List<string> SetearValoresEnXMLSinValores(List<ZurichToolsXMLNode> listaNodos, string xmlWithoutValues)
        {
            List<string> listaLineasOriginal = ObtenerListaLineas(xmlWithoutValues, '\n');

            List<string> listaLineasRetorno = new List<string>();

            foreach (var linea in listaLineasOriginal)
            {
                string lineaConValores = linea;

                foreach (var nodo in listaNodos)
                {
                    if(lineaConValores.Contains(nodo.TagName))
                    {
                        lineaConValores = lineaConValores.Replace("?", nodo.Value);
                    }
                }

                listaLineasRetorno.Add(lineaConValores);
            }

            return listaLineasRetorno;
        }

        private List<string> ObtenerListaLineas(string xmlWithoutValues, char caracterSeparacion)
        {
            return xmlWithoutValues.Split(caracterSeparacion).ToList();
        }

        private List<ZurichToolsXMLNode> ObtenerListaNodosConValores(List<string> listaTags, string xmlWithValues)
        {
            List<ZurichToolsXMLNode> listaNodos = ObtenerListaNodos(listaTags);

            SetearValoresListaNodos(listaNodos, xmlWithValues);

            return listaNodos;
        }

        private void SetearValoresListaNodos(List<ZurichToolsXMLNode> listaNodos, string xmlWithValues)
        {
            foreach (var item in listaNodos)
            {
                List<string> cadenaEntreTagsNodo = ObtenerListaCadenasEntreTagInicioFin(xmlWithValues, item.TagInicio, item.TagFin, true);

                item.Value = cadenaEntreTagsNodo.FirstOrDefault();
            }
        }

        private List<ZurichToolsXMLNode> ObtenerListaNodos(List<string> listaTags)
        {
            List<ZurichToolsXMLNode> listaNodos = new List<ZurichToolsXMLNode>();

            foreach (var item in listaTags)
            {
                ZurichToolsXMLNode zNode = new ZurichToolsXMLNode()
                {
                    TagInicio = "<" + item.Trim() + ">",
                    TagFin = "</" + item.Trim() + ">"
                };

                listaNodos.Add(zNode);
            }

            return listaNodos;
        }

        public List<string> ObtenerListaTags(string xmlString)
        {
            string source = xmlString;

            return ObtenerListaCadenasEntreTagInicioFin(xmlString, inicioTag, finTag, true);
        }

        private List<string> ObtenerListaCadenasEntreTagInicioFin(string xml, string inicioTag, string finTag, bool quitarTags)
        {
            string salida = ObtenerCadenasEntreTagInicioFin(xml, inicioTag, finTag, quitarTags);

            return salida.Split('\n').ToList();
        }

        private string ObtenerCadenasEntreTagInicioFin(string xml, string inicioTag, string finTag, bool quitarTags)
        {
            string salida = string.Empty;
            //Separa el texto de entrada en lineas
            String[] lines = xml.Split('\n');

            var regex = new Regex(@"" + inicioTag + @"(.*?)" + finTag + "");

            foreach (String item in lines)
            {
                foreach (Match match in regex.Matches(item))
                {
                    salida += match.Value + "\n";
                }
            }

            if (quitarTags)
            {
                salida = salida.Replace(finTag, string.Empty);
                salida = salida.Replace(inicioTag, string.Empty);
            }

            return salida;
        }
    }
}


