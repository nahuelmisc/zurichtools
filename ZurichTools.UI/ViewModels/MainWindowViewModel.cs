﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ZurichTools.UI.Common;
using ZurichTools.UI.Views;

namespace ZurichTools.UI.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        #region Commands

        #region AsignarPlanes

        public ICommand AsignarPlanesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.AsignarPlanes()
                );
            }
        }

        #endregion

        #region AltaUsuarioVida

        public ICommand AltaUsuarioVidaCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.AltaUsuarioVida()
                );
            }
        }

        #endregion

        #region CambiarEstadoOrden

        public ICommand CambiarEstadoOrdenCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CambiarEstadoOrden()
                );
            }
        }

        #endregion

        #region CodeReplacer

        public ICommand CodeReplacerCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CodeReplacer()
                );
            }
        }

        #endregion

        #region XMLReplacer

        public ICommand XMLReplacerCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.XMLReplacer()
                );
            }
        }

        #endregion

        #region AsignarBajaSoc

        public ICommand AsignarBajaSocCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.AsignarBajaSoc()
                );
            }
        } 

        #endregion

        #region AltaPlanes

        public ICommand AltaPlanesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.AltaPlanes()
                );
            }
        }

        #endregion

        #region ScoringIDInfoAuto

        public ICommand AltaScoringIDInfoAutoCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.AltaScoringIDInfoAuto()
                );
            }
        }

        #endregion

        #region ModificarAgenciaUsuario

        public ICommand ModificarAgenciaUsuarioCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.ModificarAgenciaUsuario()
                );
            }
        }
        
        #endregion

        #region LineComparer

        public ICommand LineComparerCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.LineComparer()
                );
            }
        }  

        #endregion

        #region AgregarTransaction

        public ICommand AgregarTransactionCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.AgregarTransaction()
                );
            }
        }

        #endregion

        #region Salir

        public ICommand SalirCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Salir()
                );
            }
        }

        #endregion

        #endregion

        #region Methods

        private void AsignarPlanes()
        {
            new AsignarPlanesView().Show();
        }

        private void AltaUsuarioVida()
        {
            new AltaUsuarioVidaView().Show();
        }

        private void CambiarEstadoOrden()
        {
            new CambiarEstadoOrdenView().Show();
        }

        private void AsignarBajaSoc()
        {
            new AsignarBajaSocView().Show();
        }

        private void AltaPlanes()
        {
            new AltaPlanesView().ShowDialog();
        }

        private void ModificarAgenciaUsuario()
        {
            new ModificarAgenciaUsuarioView().Show();
        }

        private void AltaScoringIDInfoAuto()
        {
            new AltaScoringIDInfoAutoView().ShowDialog();
        }

        private void LineComparer()
        {
            new LineComparerView().ShowDialog();
        }

        private void AgregarTransaction()
        {
            new AgregarTransactionView().Show();
        }

        private void XMLReplacer()
        {
            new XMLReplacerView().Show();
        }

        private void CodeReplacer()
        {
            new CodeReplacerView().Show();
        }

        private void Salir()
        {
            Environment.Exit(0);
        }

        #endregion
    }
}
