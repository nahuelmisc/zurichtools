﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;
using ZurichTools.UI.Common;

namespace ZurichTools.UI.ViewModels
{
    public class AltaUsuarioVidaViewModel : BaseViewModel
    {
        #region Members

        //private readonly IFileScriptProvider fileScriptProvider;

        private readonly ISaveFileDialogFactory saveFileDialogFactory;

        #endregion

        #region Constructor

        public AltaUsuarioVidaViewModel()
            : base()
        {
            //this.fileScriptProvider = new FileScriptProvider();

            this.saveFileDialogFactory = new SaveFileDialogFactory();
        }

        public override void Initialize()
        {
            base.Initialize();

            this.LimpiarCampos();
        }

        #endregion

        #region Properties

        private const String NroTicketPropertyName = "NroTicket";
        private String _NroTicket;
        public String NroTicket
        {
            get
            {
                return this._NroTicket;
            }
            set
            {
                this._NroTicket = value;
                this.NotifyPropertyChanged(NroTicketPropertyName);
            }
        }

        private const String AgenciaIDPropertyName = "AgenciaID";
        private String _AgenciaID;
        public String AgenciaID
        {
            get
            {
                return this._AgenciaID.Trim();
            }
            set
            {
                this._AgenciaID = value;
                this.NotifyPropertyChanged(AgenciaIDPropertyName);
            }
        }

        private const String NroProductorPropertyName = "NroProductor";
        private String _NroProductor;
        public String NroProductor
        {
            get
            {
                return this._NroProductor.Trim();
            }
            set
            {
                this._NroProductor = value;
                this.NotifyPropertyChanged(NroProductorPropertyName);
            }
        }

        private const String NombreProductorPropertyName = "NombreProductor";
        private String _NombreProductor;
        public String NombreProductor
        {
            get
            {
                return this._NombreProductor.Trim();
            }
            set
            {
                this._NombreProductor = value;
                this.NotifyPropertyChanged(NombreProductorPropertyName);
            }
        }

        private const String ApellidoProductorPropertyName = "ApellidoProductor";
        private String _ApellidoProductor;
        public String ApellidoProductor
        {
            get
            {
                return this._ApellidoProductor.Trim();
            }
            set
            {
                this._ApellidoProductor = value;
                this.NotifyPropertyChanged(ApellidoProductorPropertyName);
            }
        }

        private const String LoginProductorPropertyName = "LoginProductor";
        private String _LoginProductor;
        public String LoginProductor
        {
            get
            {
                return this._LoginProductor.Trim();
            }
            set
            {
                this._LoginProductor = value;
                this.NotifyPropertyChanged(LoginProductorPropertyName);
            }
        }

        private const String MailProductorPropertyName = "MailProductor";
        private String _MailProductor;
        public String MailProductor
        {
            get
            {
                return this._MailProductor.Trim();
            }
            set
            {
                this._MailProductor = value;
                this.NotifyPropertyChanged(MailProductorPropertyName);
            }
        }

        private const String UsuarioJerarquicoPropertyName = "UsuarioJerarquico";
        private String _UsuarioJerarquico;
        public String UsuarioJerarquico
        {
            get
            {
                return this._UsuarioJerarquico.Trim();
            }
            set
            {
                this._UsuarioJerarquico = value;
                this.NotifyPropertyChanged(UsuarioJerarquicoPropertyName);
            }
        }

        private const String TextoSalidaPropertyName = "TextoSalida";
        private String _TextoSalida;
        public String TextoSalida
        {
            get
            {
                return this._TextoSalida;
            }
            set
            {
                this._TextoSalida = value;
                this.NotifyPropertyChanged(TextoSalidaPropertyName);
            }
        }

        #endregion

        #region Commands

        #region Generar

        public ICommand GenerarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Generar()
                );
            }
        }

        #endregion

        #region LimpiarCampos

        public ICommand LimpiarCamposCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.LimpiarCampos()
                );
            }
        }

        #endregion

        #region CopiarPortapapeles

        public ICommand CopiarPortapapelesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CopiarPortapapeles()
                );
            }
        }

        #endregion

        #region Exportar

        public ICommand ExportarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Exportar()
                );
            }
        }

        #endregion

        #endregion

        #region Methods

        private void Generar()
        {
            try
            {
                //this.Validate();

                string textoScriptAltaUsuario = FileScriptProvider.GetTextScriptFile(Parameters.KeyFileAltaUsuarioVidaScript);

                textoScriptAltaUsuario = textoScriptAltaUsuario.Replace(Parameters.TAGAGENCIAID, this.AgenciaID);

                textoScriptAltaUsuario = textoScriptAltaUsuario.Replace(Parameters.TAGNROPRODUCTOR, this.NroProductor);

                string apellidoNombreProductor = String.Format("{0}/{1}", ApellidoProductor, NombreProductor);

                textoScriptAltaUsuario = textoScriptAltaUsuario.Replace(Parameters.TAGAPENOM, apellidoNombreProductor);

                textoScriptAltaUsuario = textoScriptAltaUsuario.Replace(Parameters.TAGLOGIN, this.LoginProductor);

                textoScriptAltaUsuario = textoScriptAltaUsuario.Replace(Parameters.TAGMAIL, this.MailProductor);

                textoScriptAltaUsuario = textoScriptAltaUsuario.Replace(Parameters.TAGUSUARIOJERARQUICO, this.UsuarioJerarquico);

                this.TextoSalida = textoScriptAltaUsuario;

            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }

        }

        //public override void Validate()
        //{
        //    List<ValidationResult> listaValidaciones = new List<ValidationResult>();

        //    if (this.NombreProductor.Trim().Equals(String.Empty))
        //    {
        //        listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.NombreProductor)));
        //    }

        //    if (this.ApellidoProductor.Trim().Equals(String.Empty))
        //    {
        //        listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.ApellidoProductor)));
        //    }

        //    if (this.LoginProductor.Trim().Equals(String.Empty))
        //    {
        //        listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.LoginProductor)));
        //    }

        //    //if (listaValidaciones.Any())
        //    //    throw new ValidationException(listaValidaciones);
        //}

        private void Exportar()
        {
            try
            {
                ValidarSalida();

                ExportToTxtIntoTransaction(TextoSalida);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        private void ValidarSalida()
        {
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (this.TextoSalida.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Salida)));
            }

            //if (listaValidaciones.Any())
            //    throw new ValidationException(listaValidaciones);
        }

        private void LimpiarCampos()
        {
            this.NroTicket = string.Empty;

            this.AgenciaID = string.Empty;

            this.NroProductor = string.Empty;

            this.NombreProductor = string.Empty;

            this.ApellidoProductor = string.Empty;

            this.LoginProductor = string.Empty;

            this.MailProductor = string.Empty;

            this.UsuarioJerarquico = string.Empty;

            this.TextoSalida = string.Empty;
        }

        private void CopiarPortapapeles()
        {
            Clipboard.SetText(TextoSalida);
        }

        #endregion
    }
}
