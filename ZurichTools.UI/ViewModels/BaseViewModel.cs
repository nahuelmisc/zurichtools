﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;
using ZurichTools.UI.Common; 

namespace ZurichTools.UI
{
    public abstract class BaseViewModel : Notify
    {
        #region Members

        private readonly ISaveFileDialogFactory saveFileDialogFactory;

        #endregion

        #region Constructor

        public BaseViewModel()
        {
            this.Initialize();

            this.saveFileDialogFactory = new SaveFileDialogFactory();
        }

        #endregion

        public virtual void Initialize() { }

        public virtual void NotifyError(string error)
        {
            MessageBox.Show(error, Labels.Error, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public virtual Boolean NotifyError(List<ValidationResult> list)
        {
            StringBuilder sb = new StringBuilder();

            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    sb.AppendLine(item.ErrorContent.ToString());
                }
                MessageBox.Show(sb.ToString(), Labels.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return true;
            }
            else
                return false;
        }

        public virtual Boolean NotifyWarning(List<ValidationResult> list)
        {

            StringBuilder sb = new StringBuilder();

            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    sb.AppendLine(item.ErrorContent.ToString());
                }
                MessageBox.Show(sb.ToString(), Labels.Error, MessageBoxButton.OK, MessageBoxImage.Warning);
                return true;
            }
            else
                return false;
        }

        //HACK: ExportToTxtIntoTransaction y EmbedIntoTransaction no serian responsabilidad del BaseViewModel
        public void ExportToTxtIntoTransaction(string output)
        { 
            ISaveFileDialog saveFileDialog = this.saveFileDialogFactory.CrearSaveDialog();

            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            saveFileDialog.FileName = String.Format("Ticket - {0}", DateTime.Now.ToString("ddMMyyyy-hhmm"));
            saveFileDialog.DefaultExt = Parameters.ExtensionTxt;
            saveFileDialog.Filter = Parameters.FilterTxt;

            String textoAGuardar = EmbedIntoTransaction(output); 

            if (saveFileDialog.ShowDialog() == true)
            {
                File.WriteAllText(saveFileDialog.FileName, textoAGuardar);
            }
        }
         
        private string EmbedIntoTransaction(string output)
        {
            String transScriptText = FileScriptProvider.GetTextScriptFile(Parameters.KeyFileTransactionScript);

            return transScriptText.Replace(Parameters.TAGTransScript, output); 
        }       
    }
}
