﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ZurichTools.CrossCutting.Resources;
using ZurichTools.Logic;
using ZurichTools.UI.Common;

namespace ZurichTools.UI.ViewModels
{
    public class AgregarTransactionViewModel : BaseViewModel
    {
        #region Members

        private readonly IEmbedIntoTransactionLogic _EmbedIntoTransactionLogic;

        private readonly ISaveFileDialogFactory saveFileDialogFactory;

        #endregion

        #region Constructor

        public AgregarTransactionViewModel()
            : base()
        {
            this._EmbedIntoTransactionLogic = new EmbedIntoTransactionLogic();

            this.saveFileDialogFactory = new SaveFileDialogFactory();
        }

        public override void Initialize()
        {
            base.Initialize();

            this.LimpiarCampos();
        }

        #endregion

        #region Properties

        private const String TextoEntradaPropertyName = "TextoEntrada";
        private String _TextoEntrada;
        public String TextoEntrada
        {
            get
            {
                return this._TextoEntrada;
            }
            set
            {
                this._TextoEntrada = value;
                this.NotifyPropertyChanged(TextoEntradaPropertyName);
            }
        }

        private const String TextoSalidaPropertyName = "TextoSalida";
        private String _TextoSalida;
        public String TextoSalida
        {
            get
            {
                return this._TextoSalida;
            }
            set
            {
                this._TextoSalida = value;
                this.NotifyPropertyChanged(TextoSalidaPropertyName);
            }
        }

        #endregion

        #region Commands

        #region Generar

        public ICommand GenerarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Generar()
                );
            }
        }

        #endregion

        #region LimpiarCampos

        public ICommand LimpiarCamposCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.LimpiarCampos()
                );
            }
        }

        #endregion

        #region CopiarPortapapeles

        public ICommand CopiarPortapapelesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CopiarPortapapeles()
                );
            }
        }

        #endregion

        #region Exportar

        public ICommand ExportarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Exportar()
                );
            }
        }

        #endregion

        #endregion

        #region Methods

        private void Generar()
        {
            try
            {
                this.TextoSalida = this._EmbedIntoTransactionLogic.GenerarScript(this.TextoEntrada);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        private void Exportar()
        {
            try
            {
                ExportToTxtIntoTransaction(TextoEntrada);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }

        }

        private void LimpiarCampos()
        {
            this.TextoEntrada = string.Empty;

            this.TextoSalida = string.Empty;
        }

        private void CopiarPortapapeles()
        {
            Clipboard.SetText(TextoSalida);
        }

        #endregion

    }
}
