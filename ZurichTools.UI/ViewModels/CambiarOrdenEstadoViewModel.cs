﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZurichTools.UI.Common;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;
using ZurichTools.Entities; 
using ZurichTools.Logic;

namespace ZurichTools.UI.ViewModels
{
    public class CambiarOrdenEstadoViewModel : BaseViewModel
    {
        #region Members

        private readonly ISaveFileDialogFactory saveFileDialogFactory;

        private CambioEstadoOrdenLogic _CambioEstadoOrdenLogic;

        #endregion

        #region Constructor

        public CambiarOrdenEstadoViewModel()
            : base()
        {
            this.saveFileDialogFactory = new SaveFileDialogFactory();

            this._CambioEstadoOrdenLogic = new CambioEstadoOrdenLogic();
        }

        public override void Initialize()
        {
            base.Initialize();

            this.LimpiarCampos();

            this.InicializarComboEstadoOrden();
        }

        private void InicializarComboEstadoOrden()
        {
            EstadoOrden p1 = new EstadoOrden() { Id = int.Parse(Parameters.ValorEstadoRechazada), Descripcion = "Rechazada" };
            EstadoOrden p2 = new EstadoOrden() { Id = int.Parse(Parameters.ValorEstadoEmitida), Descripcion = "Emitida" }; 

            this.ListaEstadoOrden = new List<EstadoOrden>() { p1, p2, };

            this.EstadoOrdenSelected = p2;
        }

        #endregion

        #region Properties

        private const String TextoEntradaPropertyName = "TextoEntrada";
        private String _TextoEntrada;
        public String TextoEntrada
        {
            get
            {
                return this._TextoEntrada;
            }
            set
            {
                this._TextoEntrada = value;
                this.NotifyPropertyChanged(TextoEntradaPropertyName);
            }
        }

        private const String TextoSalidaPropertyName = "TextoSalida";
        private String _TextoSalida;
        public String TextoSalida
        {
            get
            {
                return this._TextoSalida;
            }
            set
            {
                this._TextoSalida = value;
                this.NotifyPropertyChanged(TextoSalidaPropertyName);
            }
        }

        private const String EstadoSelectedPropertyName = "EstadoSelected";
        private EstadoOrden _EstadoSelected;
        public EstadoOrden EstadoOrdenSelected
        {
            get
            {
                return this._EstadoSelected;
            }
            set
            {
                this._EstadoSelected = value;
                this.NotifyPropertyChanged(EstadoSelectedPropertyName);
            }
        }

        private const String ListaOrdenesPropertyName = "ListaOrdenes";
        private List<EstadoOrden> _ListaOrdenes;
        public List<EstadoOrden> ListaEstadoOrden
        {
            get
            {
                return this._ListaOrdenes;
            }
            set
            {
                this._ListaOrdenes = value;
                this.NotifyPropertyChanged(ListaOrdenesPropertyName);
            }
        } 

        #endregion

        #region Commands

        #region Generar

        public ICommand GenerarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Generar()
                );
            }
        }

        #endregion

        #region Exportar

        public ICommand ExportarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Exportar()
                );
            }
        }

        #endregion

        #region LimpiarCampos

        public ICommand LimpiarCamposCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.LimpiarCampos()
                );
            }
        }

        #endregion

        #region CopiarPortapapeles

        public ICommand CopiarPortapapelesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CopiarPortapapeles()
                );
            }
        }

        #endregion

        #endregion

        #region Methods

        private void Generar()
        {
            try
            { 
                this.TextoSalida = this._CambioEstadoOrdenLogic.RetornarCodigo(this.TextoEntrada, this.EstadoOrdenSelected);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        private void Exportar()
        {
            try
            {
                ValidarSalida();

                ExportToTxtIntoTransaction(TextoSalida);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        private void ValidarSalida()
        {
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (this.TextoSalida.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Salida)));
            }

            //if (listaValidaciones.Any())
            //    throw new ValidationException(listaValidaciones);
        }

        private void LimpiarCampos()
        {
            this.TextoEntrada = String.Empty;
            this.TextoSalida = String.Empty;
        }

        private void CopiarPortapapeles()
        {
            Clipboard.SetText(TextoSalida);
        }
         
        #endregion
    }
}
