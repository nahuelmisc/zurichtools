﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;
using ZurichTools.Logic;
using ZurichTools.UI.Common;

namespace ZurichTools.UI.ViewModels
{
    public class CodeReplacerViewModel : BaseViewModel
    {
        #region Members

        private ICodeReplacerLogic _CodeReplacerLogic;

        #endregion

        #region Constructor

        public CodeReplacerViewModel()
        {
            _CodeReplacerLogic = new CodeReplacerLogic();

            this.LimpiarCampos();
        }

        #endregion

        #region Properties

        private const String TagReemplazoPropertyName = "TagReemplazo";
        private String _TagReemplazo;
        public String TagReemplazo
        {
            get
            {
                return this._TagReemplazo;
            }
            set
            {
                this._TagReemplazo = value;
                this.NotifyPropertyChanged(TagReemplazoPropertyName);
            }
        }

        private const String TextoEntradaPropertyName = "TextoEntrada";
        private String _TextoEntrada;
        public String TextoEntrada
        {
            get
            {
                return this._TextoEntrada;
            }
            set
            {
                this._TextoEntrada = value;
                this.NotifyPropertyChanged(TextoEntradaPropertyName);
            }
        }

        private const String TextoCodigoPropertyName = "TextoCodigo";
        private String _TextoCodigo;
        public String TextoCodigo
        {
            get
            {
                return this._TextoCodigo;
            }
            set
            {
                this._TextoCodigo = value;
                this.NotifyPropertyChanged(TextoCodigoPropertyName);
            }
        }

        private const String TextoSalidaPropertyName = "TextoSalida";
        private String _TextoSalida;
        public String TextoSalida
        {
            get
            {
                return this._TextoSalida;
            }
            set
            {
                this._TextoSalida = value;
                this.NotifyPropertyChanged(TextoSalidaPropertyName);
            }
        }

        private const String AgregarEspaciosPropertyName = "AgregarEspacios";
        private Boolean _AgregarEspacios;
        public Boolean AgregarEspacios
        {
            get
            {
                return this._AgregarEspacios;
            }
            set
            {
                this._AgregarEspacios = value;
                this.NotifyPropertyChanged(AgregarEspaciosPropertyName);
            }
        }

        #endregion

        #region Commands

        #region LimpiarCampos

        public ICommand LimpiarCamposCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.LimpiarCampos()
                );
            }
        }

        #endregion

        #region CopiarPortapapeles

        public ICommand CopiarPortapapelesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CopiarAlPortapapeles()
                );
            }
        }

        #endregion

        #region Generar

        public ICommand GenerarCodigoCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.GenerarCodigo()
                );
            }
        }

        #endregion

        #region ConcatenarSalida

        public ICommand ConcatenarSalidaCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.ConcatenarSalida()
                );
            }
        }

        #endregion

        #endregion

        #region Methods

        private void GenerarCodigo()
        {
            try
            {
                this.TextoSalida = this._CodeReplacerLogic.RetornarCodigoConReemplazo(this.TextoEntrada, this.TextoCodigo, this.TagReemplazo, this.AgregarEspacios);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        public void ConcatenarSalida()
        {
            this.TextoSalida = ZurichToolsTextUtils.RetornarSalidaConcatenada(TextoSalida);
        }

        public void LimpiarCampos()
        {
            this.TextoEntrada = String.Empty;
            this.TextoSalida = String.Empty;
            this.TextoCodigo = String.Empty;
            this.TagReemplazo = String.Empty;
        }

        private void CopiarAlPortapapeles()
        {
            Clipboard.SetText(TextoSalida);
        }

        #endregion
    }
}
