﻿using System;
using System.Windows;
using System.Windows.Input;
using ZurichTools.CrossCutting;
using ZurichTools.Logic;
using ZurichTools.UI.Common;

namespace ZurichTools.UI.ViewModels
{
    public class LineComparerViewModel : BaseViewModel
    {
        #region Members

        private ILineComparerLogic _LineComparerLogic;

        #endregion

        #region Constructor

        public LineComparerViewModel()
            : base()
        {
            this._LineComparerLogic = new LineComparerLogic();
        }

        #endregion

        #region Methods

        public override void Initialize()
        {
            base.Initialize();
        }

        private void CompararCodigo()
        {
            try
            { 
                this.TextoSalida = this._LineComparerLogic.RetornarComparacion(this.TextoEntrada, this.TextoComparar, CoincidirMayusMinus);
            }
            catch (ValidationException ex)
            {
                NotifyError(ex.Message);
            }
        }

        public void LimpiarCampos()
        {
            this.TextoEntrada = String.Empty;
            this.TextoComparar = String.Empty;
            this.TextoSalida = String.Empty;
        }

        private void CopiarAlPortapapeles()
        {
            Clipboard.SetText(TextoSalida);
        }

        #endregion

        #region Properties

        private const String TextoEntradaPropertyName = "TextoEntrada";
        private String _TextoEntrada = string.Empty;
        public String TextoEntrada
        {
            get
            {
                return this._TextoEntrada;
            }
            set
            {
                this._TextoEntrada = value;
                this.NotifyPropertyChanged(TextoEntradaPropertyName);
            }
        }

        private const String TextoCompararPropertyName = "TextoComparar";
        private String _TextoComparar = string.Empty;
        public String TextoComparar
        {
            get
            {
                return this._TextoComparar;
            }
            set
            {
                this._TextoComparar = value;
                this.NotifyPropertyChanged(TextoCompararPropertyName);
            }
        }

        private const String TextoSalidaPropertyName = "TextoSalida";
        private String _TextoSalida =  string.Empty;
        public String TextoSalida
        {
            get
            {
                return this._TextoSalida;
            }
            set
            {
                this._TextoSalida = value;
                this.NotifyPropertyChanged(TextoSalidaPropertyName);
            }
        }

        private const String CoincidirMayusMinusPropertyName = "CoincidirMayusMinus";
        private Boolean _CoincidirMayusMinus = false;
        public Boolean CoincidirMayusMinus
        {
            get
            {
                return this._CoincidirMayusMinus;
            }
            set
            {
                this._CoincidirMayusMinus = value;
                this.NotifyPropertyChanged(CoincidirMayusMinusPropertyName);
            }
        }

        #endregion

        #region Commands

        #region CompararCodigo

        public ICommand CompararCodigoCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CompararCodigo()
                );
            }
        }

        #endregion

        #region LimpiarCampos

        public ICommand LimpiarCamposCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.LimpiarCampos()
                );
            }
        }

        #endregion

        #region CopiarPortapapeles

        public ICommand CopiarPortapapelesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CopiarAlPortapapeles()
                );
            }
        }

        #endregion
        
        #endregion

    }
}
