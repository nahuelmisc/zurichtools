﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;
using ZurichTools.Logic;
using ZurichTools.UI.Common;

namespace ZurichTools.UI.ViewModels
{
    public class ModificarAgenciaUsuarioViewModel : BaseViewModel
    {
        #region Members

        private readonly IModificacionAgenciaUsuarioLogic _ModificacionAgenciaUsuarioLogic;

        #endregion

        #region Constructor

        public ModificarAgenciaUsuarioViewModel()
            : base()
        {
            this._ModificacionAgenciaUsuarioLogic = new ModificacionAgenciaUsuarioLogic();
        }

        #endregion

        #region Properties

        private const String AgenciaNombrePropertyName = "AgenciaNombre";
        private String _AgenciaNombre;
        public String AgenciaNombre
        {
            get
            {
                return this._AgenciaNombre;
            }
            set
            {
                this._AgenciaNombre = value;
                this.NotifyPropertyChanged(AgenciaNombrePropertyName);
            }
        }

        private const String UsuariosPropertyName = "Usuarios";
        private String _Usuarios;
        public String Usuarios
        {
            get
            {
                return this._Usuarios;
            }
            set
            {
                this._Usuarios = value;
                this.NotifyPropertyChanged(UsuariosPropertyName);
            }
        }

        private const String TextoSalidaPropertyName = "TextoSalida";
        private String _TextoSalida;
        public String TextoSalida
        {
            get
            {
                return this._TextoSalida;
            }
            set
            {
                this._TextoSalida = value;
                this.NotifyPropertyChanged(TextoSalidaPropertyName);
            }
        }

        #endregion

        #region Commands

        #region Generar

        public ICommand GenerarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Generar()
                );
            }
        }

        #endregion

        #region LimpiarCampos

        public ICommand LimpiarCamposCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.LimpiarCampos()
                );
            }
        }

        #endregion

        #region CopiarPortapapeles

        public ICommand CopiarPortapapelesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CopiarPortapapeles()
                );
            }
        }

        #endregion

        #region Exportar

        public ICommand ExportarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Exportar()
                );
            }
        }

        #endregion

        #endregion

        #region Methods

        public override void Initialize()
        {
            base.Initialize();

            this.LimpiarCampos();
        }

        private void Generar()
        {
            try
            {
                this.TextoSalida = this._ModificacionAgenciaUsuarioLogic.GenerarScript(this.AgenciaNombre, this.Usuarios);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        private void LimpiarCampos()
        {
            this.AgenciaNombre = String.Empty;
            this.Usuarios = String.Empty;
            this.TextoSalida = String.Empty;
        }

        private void CopiarPortapapeles()
        {
            Clipboard.SetText(TextoSalida);
        }

        private void Exportar()
        {
            try
            {
                ValidarSalida();

                ExportToTxtIntoTransaction(TextoSalida);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        private void ValidarSalida()
        {
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (this.TextoSalida.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Salida)));
            }

            if (listaValidaciones.Any())
                throw new ValidationException(listaValidaciones);
        }

        

        #endregion
    }
}
