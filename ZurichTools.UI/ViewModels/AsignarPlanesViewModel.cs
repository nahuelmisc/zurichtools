﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;
using ZurichTools.Logic;
using ZurichTools.UI.Common;

namespace ZurichTools.UI.ViewModels
{
    public class AsignarPlanesViewModel : BaseViewModel
    {
        #region Members

        private readonly ISaveFileDialogFactory saveFileDialogFactory;

        private readonly IAsignarPlanesLogic _AsignarPlanesLogic;

        #endregion

        #region Constructor

        public AsignarPlanesViewModel()
            : base()
        {
            this._AsignarPlanesLogic = new AsignarPlanesLogic();

            this.saveFileDialogFactory = new SaveFileDialogFactory();
        }

        public override void Initialize()
        {
            base.Initialize();

            this.LimpiarCampos();
        }

        #endregion

        #region Properties

        private const String DescripcionPlanPropertyName = "DescripcionPlan";
        private String _DescripcionPlan;
        public String DescripcionPlan
        {
            get
            {
                return this._DescripcionPlan.Trim();
            }
            set
            {
                this._DescripcionPlan = value;
                this.NotifyPropertyChanged(DescripcionPlanPropertyName);
            }
        }

        private const String TextoEntradaPropertyName = "TextoEntrada";
        private String _TextoEntrada;
        public String TextoEntrada
        {
            get
            {
                return this._TextoEntrada;
            }
            set
            {
                this._TextoEntrada = value;
                this.NotifyPropertyChanged(TextoEntradaPropertyName);
            }
        }

        private const String TextoSalidaPropertyName = "TextoSalida";
        private String _TextoSalida;
        public String TextoSalida
        {
            get
            {
                return this._TextoSalida;
            }
            set
            {
                this._TextoSalida = value;
                this.NotifyPropertyChanged(TextoSalidaPropertyName);
            }
        }

        #endregion

        #region Commands

        #region Generar

        public ICommand GenerarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Generar()
                );
            }
        }

        #endregion

        #region Exportar

        public ICommand ExportarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Exportar()
                );
            }
        }

        #endregion

        #region LimpiarCampos

        public ICommand LimpiarCamposCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.LimpiarCampos()
                );
            }
        }

        #endregion

        #region CopiarPortapapeles

        public ICommand CopiarPortapapelesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CopiarPortapapeles()
                );
            }
        }

        #endregion

        #endregion

        #region Methods

        private void Generar()
        {
            try
            { 
                this.TextoSalida = this._AsignarPlanesLogic.GenerarScript(this.DescripcionPlan, this.TextoEntrada);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        private void Exportar()
        {
            try
            {
                ValidarSalida();

                ExportToTxtIntoTransaction(TextoSalida);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        private void ValidarSalida()
        {
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (this.TextoSalida.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Salida)));
            }

            if (listaValidaciones.Any())
                throw new ValidationException(listaValidaciones);
        }

        private void LimpiarCampos()
        {
            this.DescripcionPlan = String.Empty;
            this.TextoEntrada = String.Empty;
            this.TextoSalida = String.Empty;
        }

        private void CopiarPortapapeles()
        {
            Clipboard.SetText(TextoSalida);
        }

        #endregion
    }
}
