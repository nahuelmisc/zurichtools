﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;
using ZurichTools.Logic;
using ZurichTools.UI.Common;

namespace ZurichTools.UI.ViewModels
{
    public class AltaScoringIDInfoAutoViewModel : BaseViewModel
    {
        #region Members

        private readonly IAltaScoringIDInfoAutoLogic _AltaScoringIDInfoAutoLogic;

        #endregion

        #region Constructor

        public AltaScoringIDInfoAutoViewModel()
            : base()
        {
            this._AltaScoringIDInfoAutoLogic = new AltaScoringIDInfoAutoLogic();
        }

        public override void Initialize()
        {
            base.Initialize();

            this.LimpiarCampos();
        }

        #endregion

        #region Properties

        private const String UltimoScoringIDPropertyName = "UltimoScoringID";
        private String _UltimoScoringID;
        public String UltimoScoringID
        {
            get
            {
                return this._UltimoScoringID.Trim();
            }
            set
            {
                this._UltimoScoringID = value;
                this.NotifyPropertyChanged(UltimoScoringIDPropertyName);
            }
        }

        private const String CodigosInfoAutoPropertyName = "CodigosInfoAuto";
        private String _CodigosInfoAuto;
        public String CodigosInfoAuto
        {
            get
            {
                return this._CodigosInfoAuto;
            }
            set
            {
                this._CodigosInfoAuto = value;
                this.NotifyPropertyChanged(CodigosInfoAutoPropertyName);
            }
        }

        private const String TextoSalidaPropertyName = "TextoSalida";
        private String _TextoSalida;
        public String TextoSalida
        {
            get
            {
                return this._TextoSalida;
            }
            set
            {
                this._TextoSalida = value;
                this.NotifyPropertyChanged(TextoSalidaPropertyName);
            }
        }

        #endregion

        #region Methods

        private void Generar()
        {
            try
            { 
                this.TextoSalida = this._AltaScoringIDInfoAutoLogic.GenerarScript(this.UltimoScoringID, this.CodigosInfoAuto);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        } 

        private void Exportar()
        {
            try
            {
                ValidarSalida();

                ExportToTxtIntoTransaction(TextoSalida);
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        private void ValidarSalida()
        {
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (this.TextoSalida.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Salida)));
            }

            if (listaValidaciones.Any())
                throw new ValidationException(listaValidaciones);
        }

        private void LimpiarCampos()
        {
            this.UltimoScoringID = String.Empty;
            this.CodigosInfoAuto = String.Empty;
            this.TextoSalida = String.Empty;
        }

        private void CopiarPortapapeles()
        {
            Clipboard.SetText(TextoSalida);
        }

        #endregion

        #region Commands

        #region Generar

        public ICommand GenerarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Generar()
                );
            }
        }

        #endregion

        #region Exportar

        public ICommand ExportarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Exportar()
                );
            }
        }

        #endregion

        #region LimpiarCampos

        public ICommand LimpiarCamposCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.LimpiarCampos()
                );
            }
        }

        #endregion

        #region CopiarPortapapeles

        public ICommand CopiarPortapapelesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CopiarPortapapeles()
                );
            }
        }

        #endregion

        #endregion
    }
}
