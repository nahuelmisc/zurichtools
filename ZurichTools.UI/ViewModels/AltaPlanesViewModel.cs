﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ZurichTools.CrossCutting;
using ZurichTools.CrossCutting.Resources;
using ZurichTools.UI.Common;

namespace ZurichTools.UI.ViewModels
{
    public class AltaPlanesViewModel : BaseViewModel
    {
        #region Members

        //private readonly IFileScriptProvider fileScriptProvider;

        private readonly ISaveFileDialogFactory saveFileDialogFactory;

        #endregion

        #region Constructor

        public AltaPlanesViewModel()
            : base()
        {
            //this.fileScriptProvider = new FileScriptProvider();

            this.saveFileDialogFactory = new SaveFileDialogFactory();
        }

        public override void Initialize()
        {
            base.Initialize();

            this.LimpiarCampos();
        }

        #endregion

        #region Properties

        private const String NumeroPolizaPropertyName = "NumeroPoliza";
        private String _NumeroPoliza;
        public String NumeroPoliza
        {
            get
            {
                return this._NumeroPoliza;
            }
            set
            {
                this._NumeroPoliza = value;
                this.NotifyPropertyChanged(NumeroPolizaPropertyName);
            }
        }


        private const String DescripcionPlanPropertyName = "DescripcionPlan";
        private String _DescripcionPlan;
        public String DescripcionPlan
        {
            get
            {
                return this._DescripcionPlan;
            }
            set
            {
                this._DescripcionPlan = value;
                this.NotifyPropertyChanged(DescripcionPlanPropertyName);
            }
        }


        private const String RiesgoPlanPropertyName = "RiesgoPlan";
        private String _RiesgoPlan;
        public String RiesgoPlan
        {
            get
            {
                return this._RiesgoPlan;
            }
            set
            {
                this._RiesgoPlan = value;
                this.NotifyPropertyChanged(RiesgoPlanPropertyName);
            }
        }


        private const String DescripcionPlanPagoPropertyName = "DescripcionPlanPago";
        private String _DescripcionPlanPago;
        public String DescripcionPlanPago
        {
            get
            {
                return this._DescripcionPlanPago;
            }
            set
            {
                this._DescripcionPlanPago = value;
                this.NotifyPropertyChanged(DescripcionPlanPagoPropertyName);
            }
        }


        private const String SociedadNombrePropertyName = "SociedadNombre";
        private String _SociedadNombre;
        public String SociedadNombre
        {
            get
            {
                return this._SociedadNombre;
            }
            set
            {
                this._SociedadNombre = value;
                this.NotifyPropertyChanged(SociedadNombrePropertyName);
            }
        }


        private const String DescripcionSegmentoPropertyName = "DescripcionSegmento";
        private String _DescripcionSegmento;
        public String DescripcionSegmento
        {
            get
            {
                return this._DescripcionSegmento;
            }
            set
            {
                this._DescripcionSegmento = value;
                this.NotifyPropertyChanged(DescripcionSegmentoPropertyName);
            }
        }


        private const String IdTomadorPropertyName = "IdTomador";
        private String _IdTomador;
        public String IdTomador
        {
            get
            {
                return this._IdTomador;
            }
            set
            {
                this._IdTomador = value;
                this.NotifyPropertyChanged(IdTomadorPropertyName);
            }
        }


        private const String InstitucionesFinancierasPropertyName = "InstitucionesFinancieras";
        private String _InstitucionesFinancieras;
        public String InstitucionesFinancieras
        {
            get
            {
                return this._InstitucionesFinancieras;
            }
            set
            {
                this._InstitucionesFinancieras = value;
                this.NotifyPropertyChanged(InstitucionesFinancierasPropertyName);
            }
        } 

        private const String TextoSalidaPropertyName = "TextoSalida";
        private String _TextoSalida;
        public String TextoSalida
        {
            get
            {
                return this._TextoSalida;
            }
            set
            {
                this._TextoSalida = value;
                this.NotifyPropertyChanged(TextoSalidaPropertyName);
            }
        }

        #endregion

        #region Commands

        #region Generar

        public ICommand GenerarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Generar()
                );
            }
        }

        #endregion

        #region Exportar

        public ICommand ExportarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Exportar()
                );
            }
        }

        #endregion

        #region LimpiarCampos

        public ICommand LimpiarCamposCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.LimpiarCampos()
                );
            }
        }

        #endregion

        #region CopiarPortapapeles

        public ICommand CopiarPortapapelesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CopiarPortapapeles()
                );
            }
        }

        #endregion

        #endregion

        #region Methods

        private void Generar()
        {
            try
            {
                //this.Validate();

                //TODO: to do, do it!

                //String entrada = this.TextoEntrada;

                //string codigoAltaPlanes = this.fileScriptProvider.GetTextScriptFile(Parameters.KeyFileAltaPlanes);

                //codigoAltaPlanes = codigoAltaPlanes.Replace(Parameters.TAGIDSOC, entrada);

                //this.TextoSalida = codigoAltaPlanes;
            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        private void Exportar()
        {
            try
            {
                ValidarSalida();

                String transScriptText = FileScriptProvider.GetTextScriptFile(Parameters.KeyFileTransactionScript);

                ISaveFileDialog saveFileDialog = this.saveFileDialogFactory.CrearSaveDialog();

                saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                saveFileDialog.FileName = String.Format("T- {0}", DateTime.Now.ToString("ddMMyyyy-hhmm"));
                saveFileDialog.DefaultExt = Parameters.ExtensionTxt;
                saveFileDialog.Filter = Parameters.FilterTxt;

                String textoAGuardar = transScriptText.Replace(Parameters.TAGTransScript, this.TextoSalida);

                if (saveFileDialog.ShowDialog() == true)
                {
                    File.WriteAllText(saveFileDialog.FileName, textoAGuardar);
                }

            }
            catch (Exception ex)
            {
                NotifyError(ex.Message);
            }
        }

        private void ValidarSalida()
        {
            List<ValidationResult> listaValidaciones = new List<ValidationResult>();

            if (this.TextoSalida.Trim().Equals(String.Empty))
            {
                listaValidaciones.Add(new ValidationResult(false, String.Format(Messages.ElCampoXNoPuedeEstarVacio, Labels.Salida)));
            }

            //if (listaValidaciones.Any())
            //    throw new ValidationException(listaValidaciones);
        }

        private void LimpiarCampos()
        {

            //this.TextoEntrada = String.Empty;
            this.TextoSalida = String.Empty;
        }

        private void CopiarPortapapeles()
        {
            Clipboard.SetText(TextoSalida);
        }


        #endregion
    }
}
