﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ZurichTools.Logic;
using ZurichTools.UI.Common;

namespace ZurichTools.UI.ViewModels
{
    public class XMLReplacerViewModel : BaseViewModel
    {
        #region Members

        private XMLReplacerLogic _XMLReplacerLogic;

        #endregion

        #region Constructor

        public XMLReplacerViewModel()
        {
            this._XMLReplacerLogic = new XMLReplacerLogic();
        }

        #endregion

        #region Properties

        private const String TextoEntradaPropertyName = "TextoEntrada";
        private String _TextoEntrada;
        public String TextoEntrada
        {
            get
            {
                return this._TextoEntrada;
            }
            set
            {
                this._TextoEntrada = value;
                this.NotifyPropertyChanged(TextoEntradaPropertyName);
            }
        }

        private const String TextoCompararPropertyName = "TextoComparar";
        private String _TextoComparar;
        public String TextoComparar
        {
            get
            {
                return this._TextoComparar;
            }
            set
            {
                this._TextoComparar = value;
                this.NotifyPropertyChanged(TextoCompararPropertyName);
            }
        }

        private const String TextoSalidaPropertyName = "TextoSalida";
        private String _TextoSalida;
        public String TextoSalida
        {
            get
            {
                return this._TextoSalida;
            }
            set
            {
                this._TextoSalida = value;
                this.NotifyPropertyChanged(TextoSalidaPropertyName);
            }
        }




        #endregion

        #region Methods

        public override void Initialize()
        {
            base.Initialize();
        }

        private void Reemplazar()
        {
            this.TextoSalida = this._XMLReplacerLogic.ReemplazarValores(this.TextoEntrada, this.TextoComparar);
        }

        public void LimpiarCampos()
        {
            this.TextoEntrada = String.Empty;
            this.TextoComparar = String.Empty;
            this.TextoSalida = String.Empty;
        }

        private void CopiarAlPortapapeles()
        {
            Clipboard.SetText(TextoSalida);
        }

        #endregion

        #region Commands

        #region Reemplazar

        public ICommand ReemplazarCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.Reemplazar()
                );
            }
        }

        #endregion

        #region LimpiarCampos

        public ICommand LimpiarCamposCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.LimpiarCampos()
                );
            }
        }

        #endregion

        #region CopiarPortapapeles

        public ICommand CopiarPortapapelesCommand
        {
            get
            {
                return new RelayCommand
                (
                    a => this.CopiarAlPortapapeles()
                );
            }
        }

        #endregion

        #endregion
    }
}
