﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZurichTools.UI
{
    public class SaveFileDialogFactory : ISaveFileDialogFactory
    {
        public ISaveFileDialog CrearSaveDialog()
        {
            return new SaveFileDialog();
        }
    }
}
