﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZurichTools.UI
{
    public class SaveFileDialog : ISaveFileDialog
    {
        private readonly Microsoft.Win32.SaveFileDialog saveFileDialog;

        public SaveFileDialog()
        {
            this.saveFileDialog = new Microsoft.Win32.SaveFileDialog();
        }

        public String InitialDirectory
        {
            get
            {
                return this.saveFileDialog.InitialDirectory;
            }
            set
            {
                this.saveFileDialog.InitialDirectory = value;
            }
        }

        public String DefaultExt
        {
            get
            {
                return this.saveFileDialog.DefaultExt;
            }
            set
            {
                this.saveFileDialog.DefaultExt = value;
            }
        }

        public String Filter
        {
            get
            {
                return this.saveFileDialog.Filter;
            }
            set
            {
                this.saveFileDialog.Filter = value;
            }
        }

        public String FileName
        {
            get
            {
                return this.saveFileDialog.FileName;
            }
            set
            {
                this.saveFileDialog.FileName = value;
            }
        }

        public Boolean? ShowDialog()
        {
            return this.saveFileDialog.ShowDialog();
        }
    }

}
