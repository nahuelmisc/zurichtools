﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ZurichTools.UI.ViewModels;

namespace ZurichTools.UI.Views
{
    /// <summary>
    /// Interaction logic for ModificarAgenciaUsuarioView.xaml
    /// </summary>
    public partial class ModificarAgenciaUsuarioView : Window
    {
        public ModificarAgenciaUsuarioView()
        {
            InitializeComponent();
            this.DataContext = new ModificarAgenciaUsuarioViewModel();
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
