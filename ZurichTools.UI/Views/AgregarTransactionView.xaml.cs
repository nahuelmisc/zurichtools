﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ZurichTools.UI.ViewModels;

namespace ZurichTools.UI.Views
{
    /// <summary>
    /// Interaction logic for AgregarTransactionView.xaml
    /// </summary>
    public partial class AgregarTransactionView : Window
    {
        public AgregarTransactionView()
        {
            InitializeComponent();
            this.DataContext = new AgregarTransactionViewModel();
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
