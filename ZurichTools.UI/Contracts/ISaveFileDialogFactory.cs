﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZurichTools.UI
{
    public interface ISaveFileDialogFactory
    {
        ISaveFileDialog CrearSaveDialog();
    }
}
