﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZurichTools.UI
{
    public interface ISaveFileDialog
    {
        String InitialDirectory { get; set; }

        String DefaultExt { get; set; }

        String Filter { get; set; }

        String FileName { get; set; }

        Boolean? ShowDialog();
    }
}
