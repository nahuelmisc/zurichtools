﻿DECLARE @DescripcionAgenciaNew VARCHAR(50) = 'TAGDESCRIPCION'; 

DECLARE @LoginUsuariosTable TABLE(LoginUsuario VARCHAR(10));
DECLARE @IDUsuariosTable TABLE(IDUsuario VARCHAR(50));
DECLARE @DescripcionTipoAgente VARCHAR(50) = 'Productor';

INSERT INTO @LoginUsuariosTable(LoginUsuario)
VALUES TAGUSUARIOS

INSERT INTO @IDUsuariosTable(IDUsuario)
SELECT ID_Usuario FROM t_cot_Usuario WHERE Activo_Auditoria = 1 AND Login_Usuario IN (SELECT LoginUsuario FROM @LoginUsuariosTable)
 
DECLARE @IDTipoAgenteUsuario INT
SELECT @IDTipoAgenteUsuario = TipoAgente_ID FROM t_prm_TipoAgente WHERE TipoAgente_Descripcion = @DescripcionTipoAgente	
SELECT @IDTipoAgenteUsuario

DECLARE @IDAgenciaNew INT
SELECT @IDAgenciaNew = Agencia_ID FROM t_lif_Agencia WHERE Agencia_Descripcion = @DescripcionAgenciaNew			
SELECT @IDAgenciaNew  

--Se asocian todos los telemarketers 
DECLARE @IDUSUARIO_CURSOR VARCHAR(50)

DECLARE USUARIOS_CURSOR CURSOR fast_forward
FOR SELECT IdUsuario FROM @IDUsuariosTable

OPEN USUARIOS_CURSOR

FETCH USUARIOS_CURSOR INTO @IDUSUARIO_CURSOR
WHILE (@@FETCH_STATUS = 0)
BEGIN 

	UPDATE t_lif_AgenciaUsuario
	SET Agencia_ID = @IDAgenciaNew
	WHERE ID_Usuario = @IDUSUARIO_CURSOR
	AND Activo_Auditoria = 1

	UPDATE t_lif_AgenciaTipoAgente
	SET Agencia_ID = @IDAgenciaNew
	WHERE ID_Usuario = @IDUSUARIO_CURSOR
	AND TipoAgente_ID = @IDTipoAgenteUsuario 
	AND Activo_Auditoria = 1

	UPDATE t_lif_UsuarioPlan
	SET Agencia_ID = @IDAgenciaNew
	WHERE ID_Usuario = @IDUSUARIO_CURSOR
	AND Activo_Auditoria = 1
  
	FETCH USUARIOS_CURSOR INTO @IDUSUARIO_CURSOR

END

CLOSE USUARIOS_CURSOR

DEALLOCATE USUARIOS_CURSOR