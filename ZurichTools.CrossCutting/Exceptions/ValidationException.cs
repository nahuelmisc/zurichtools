﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls; 

namespace ZurichTools.CrossCutting
{
    public class ValidationException : Exception
    {
        private List<ValidationResult> listaValidaciones;

        public ValidationException(List<ValidationResult> listaValidaciones)
        { 
            this.listaValidaciones = listaValidaciones;
        }

        public override string Message
        {
            get
            {
                return GenerateExceptionMessage(this.listaValidaciones);
            }
        }

        private static string GenerateExceptionMessage(IEnumerable<ValidationResult> validationResults)
        {
            var exceptionMessage = String.Empty;
            if (validationResults != null)
            {
                StringBuilder sb = new StringBuilder();
                foreach (ValidationResult vr in validationResults)
                {
                    sb.Append(vr.ErrorContent.ToString());
                    sb.Append(Environment.NewLine);
                }
                exceptionMessage = sb.ToString();
            }
            return exceptionMessage;
        }
    }
}
