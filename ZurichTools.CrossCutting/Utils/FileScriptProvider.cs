﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ZurichTools.CrossCutting.Resources;

namespace ZurichTools.CrossCutting
{
    public static class FileScriptProvider
    {
        public static string GetTextScriptFile(string fileKey)
        {
            var assembly = Assembly.GetExecutingAssembly();
            string assemblyName = assembly.GetName().Name;
            var resourceFileName = fileKey; // "MyCompany.MyProduct.MyFile.txt";
            string resourceFolder = Parameters.ResourcesFiles;
            var resourceName = assemblyName + '.' + resourceFolder + '.' + fileKey + Parameters.ExtensionPuntoTxt;

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream, System.Text.Encoding.Default))
            {
                string result = reader.ReadToEnd();
                return result;
            }
        }
    }
}
