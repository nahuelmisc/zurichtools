﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZurichTools.CrossCutting
{
    public static class ZurichToolsTextUtils
    {
        #region Enum

        public enum TipoCadena
        {
            Varchar,
            Int
        };

        #endregion

        public static string[] RemoveEmptyLines(string[] lines)
        {
            var listLines = lines.ToList();

            var linesToRemove = new List<string>();

            foreach (var l in lines.ToList())
            {
                if (string.IsNullOrEmpty(l.Trim()))
                    linesToRemove.Add(l);
            }

            listLines.RemoveAll(x => linesToRemove.Contains(x));

            return listLines.ToArray();
        }

        public static string RetornarSalidaConcatenada(string TextoSalida)
        {
            string salidaConcatenada = string.Empty;

            TextoSalida = TextoSalida.Replace('\r', ' ');

            String[] lines = TextoSalida.Split('\n');

            foreach (var line in lines)
            {
                salidaConcatenada += line;
            }

            return salidaConcatenada;
        }

        public static string GenerarCadenaInsertMultiple(string textoEntrada, TipoCadena tipoCadena)
        {

            string salida = GenerarCadenaInsertCampos(textoEntrada, TipoCadena.Varchar);

            string salidaFinal = RemoveLastCharacter(salida);

            return salidaFinal;
        }

        private static string GenerarCadenaInsertCampos(string textoEntrada, TipoCadena tipoCadena)
        {
            string[] arregloTexto = textoEntrada.Split('\n');

            arregloTexto = ZurichToolsTextUtils.RemoveEmptyLines(arregloTexto);

            string acumuladorResultado = string.Empty;

            string cadenaInicio = string.Empty;

            string cadenaFinal = string.Empty;

            switch (tipoCadena)
            {
                case TipoCadena.Varchar:
                    cadenaInicio = "('";
                    cadenaFinal = "'),";
                    break;
                case TipoCadena.Int:
                    cadenaInicio = "(";
                    cadenaFinal = "),";
                    break;
                default:
                    break;
            }

            foreach (var campo in arregloTexto)
            {
                acumuladorResultado = acumuladorResultado + cadenaInicio + campo.Trim() + cadenaFinal;
            }

            return acumuladorResultado;
        }

        private static string RemoveLastCharacter(string cadena)
        {
            int longitudCadenaFinal = cadena.Length;

            cadena = cadena.Substring(0, longitudCadenaFinal - 1);

            return cadena;
        }

    }
}
