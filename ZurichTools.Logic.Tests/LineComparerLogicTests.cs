﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZurichTools.Logic.Tests
{
    [TestClass]
    public class LineComparerLogicTests
    {
        public LineComparerLogic Target;

        [TestInitialize]
        public virtual void Initialize()
        {
            Target = new LineComparerLogic();
        } 

        [TestClass]
        public class ElMetodo_RetornarComparacion : LineComparerLogicTests
        {
            #region Members

            string entrada;
            string textoAComparar;
            bool coincidirMayusMinus;

            #endregion

            [TestInitialize]
            public override void Initialize()
            {
                base.Initialize();

                this.textoAComparar = "Uno\nDos\nTres\nCuatro\nCinco\n";
            }

            [TestMethod]
            public void Retorna_string_vacio_si_encuentra_todas_las_lineas_CoincidirMayusMinus_false()
            {
                //Arrange
                this.entrada = "Cinco\nDos\nUno\nCuatro\nTres\n";
                this.coincidirMayusMinus = false;

                //Act
                var resultado = this.Target.RetornarComparacion(entrada, textoAComparar, coincidirMayusMinus);

                //Assert
                Assert.AreEqual(resultado, string.Empty);
            }

            [TestMethod]
            public void Retorna_string_vacio_si_encuentra_todas_las_lineas_CoincidirMayusMinus_true()
            {
                //Arrange
                this.entrada = "Cinco\nCuatro\nUno\nTres\nDos\n";
                this.coincidirMayusMinus = false;

                //Act
                var resultado = this.Target.RetornarComparacion(entrada, textoAComparar, coincidirMayusMinus);

                //Assert
                Assert.AreEqual(resultado, string.Empty);
            }

            [TestMethod]
            public void Retorna_la_concatenacion_de_las_lineas_que_no_coinciden_en_el_texto_a_comparar_CoincidirMayusMinus_false()
            {
                //Arrange
                this.entrada = "Tres\nCinco\nDos\n";
                this.coincidirMayusMinus = false;

                //Act
                var resultado = this.Target.RetornarComparacion(entrada, textoAComparar, coincidirMayusMinus);

                //Assert
                Assert.AreEqual(resultado, "UnoCuatro");
            }

            [TestMethod]
            public void Retorna_string_vacio_si_encuentra_todas_las_lineas_CoincidirMayusMinus_false_cuando_estas_NO_estan_en_Orden()
            {
                //Arrange
                this.entrada = "Dos\nUno\nTres\nCinco\nCuatro\n";
                this.coincidirMayusMinus = false;

                //Act
                var resultado = this.Target.RetornarComparacion(entrada, textoAComparar, coincidirMayusMinus);

                //Assert
                Assert.AreEqual(resultado, string.Empty);
            }
        }
    }
}
