﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZurichTools.Entities
{
    public class ZurichToolsXMLNode
    {
        #region Properties

        private string _TagInicio;

        public string TagInicio
        {
            get { return _TagInicio; }
            set { _TagInicio = value; }
        }


        public string TagFin { get; set; }

        public string Value { get; set; }

        public string TagName
        {
            //TODO: UT
            get
            {
                string tagName = _TagInicio;

                tagName = tagName.Replace("<", string.Empty);

                tagName = tagName.Replace(">", string.Empty);

                return tagName;
            }
        }

        #endregion
    }
}
